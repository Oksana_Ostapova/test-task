import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

import 'package:unsplashimages/photo_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    this.getJsonData();
    super.initState();
  }

  List data;
  Future<String> getJsonData() async{
    try{
      var response = await http.get('https://api.unsplash.com/photos?per_page=30&client_id=elRx1MbBWTba9-vNs5v2oGK8iw_dfHKtMPGdxCmp0TQ');
      setState(() {
        data = json.decode(response.body);
      });
    }
    catch(e){}
    return 'success';
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Unsplash gallery"),
        ),
        body:
          data != null ? new ListView.builder(
            itemCount: data.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[

                        new Card(
                            child: InkWell(
                              child: new Container(
                                padding: EdgeInsets.all(20.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[

                                    Image.network(
                                      data[index]['urls']['small'],
                                    ),

                                    Text(
                                      data[index]['user']['name'].toString(),
                                      style: TextStyle(
                                        fontSize: 20,

                                      ),
                                    ),

                                  ],
                                ),
                              ),
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) =>
                                        Photo(text: data[index]['user']['name']
                                            .toString(),
                                          data: data[index]['urls']['regular'],)
                                ),
                                );
                              },
                            )
                        )
                      ],
                    )
                ),

              );
            }): new Center(child:new CircularProgressIndicator(
              strokeWidth: 3.0)),
      )
    );

  }
}

