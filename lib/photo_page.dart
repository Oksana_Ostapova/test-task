import 'package:flutter/material.dart';

class Photo extends StatelessWidget {
  final String text;
  final String data;

  Photo({Key key, @required this.text, @required this.data}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: ()=>Navigator.pop(context),
            ),
            title: Text(text),
          ),
          body: data != null ? Container(
            child: Image.network(
              data,
              fit: BoxFit.contain,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),

        ): new Center(child:new CircularProgressIndicator(
              strokeWidth: 3.0)),
      ),
    );
  }
}
